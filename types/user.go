package types

import (
	"gopkg.in/mgo.v2/bson"
	"../database"
	"crypto/sha256"
)


type User struct {
	Id 		bson.ObjectId 	`json:"-"     schema:"id"       bson:"_id,omitempty"`
	Login 		string 		`json:"login" schema:"login"    bson:"login"`
	Email 		string 		`json:"email" schema:"email"    bson:"email"`
	Password 	string 		`json:"-"     schema:"password" bson:"password"`
	Imei 		string 		`json:"imei"  schema:"imei"     bson:"imei"`
}

func (u *User) CheckUnique() bool {
	err := false
	if c,_ := database.Users.Find(bson.M{"login":u.Login}).Count(); c != 0 {
		err = true
	}
	if c,_ := database.Users.Find(bson.M{"email":u.Email}).Count(); c != 0 {
		err = true
	}
	return !err
}

func (u *User) HashPassword() {
	password := sha256.Sum256([]byte(u.Password))
	u.Password = string(password[:])
}
