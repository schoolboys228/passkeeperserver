package types

import (
	"gopkg.in/mgo.v2/bson"
	"time"
	"../database"
)

type Type int

const (
	WEB Type = iota
	DESKTOP
	MOBILE
)

type Token struct {
	Id 		bson.ObjectId 	`json:"-"     bson:"_id,omitempty" schema:"_id"`
	Token 		string 		`json:"token" bson:"token"         schema:"token"`
	UserID 		bson.ObjectId 	`json:"-"     bson:"user_id"       schema:"user_id"`
	ExpiresAt 	time.Time 	`json:"-"     bson:"expires_at"    schema:"expires_at"`
	Type		Type 		`json:"-"     bson:"type"          schema:"type"`
}

func (t *Token) CheckUnique() bool {
	err := false
	if count, _ := database.Tokens.Find(bson.M{"token": t.Token}).Count(); count != 0 {
		err = true
	}
	return !err
}

func (t *Token) Valid(_type Type) (*Token, error){
	err := database.Tokens.Find(bson.M{"token": t.Token, "type": _type}).One(&t)
	return t, err
}

func (t *Token) GetUser() User {
	var user User
	database.Users.Find(bson.M{"_id": t.UserID}).One(&user);
	return user
}