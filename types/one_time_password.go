package types

import (
	"gopkg.in/mgo.v2/bson"
	"time"
	"math/rand"
	"strconv"
)

type OneTimePass struct {
	Id 		bson.ObjectId 	`bson:"_id,omitempty" json:"-"`
	Code 		string 		`bson:"code"          json:"code"`
	ExpiresAt 	time.Time 	`bson:"expires_at"    json:"expires_at"`
	UserId 		bson.ObjectId 	`bson:"user_id"       json:"-"`
}

func (o *OneTimePass) Gen() {
	o.Code = strconv.Itoa(int(rand.Uint32() % 10)) + strconv.Itoa(int(rand.Uint32() % 10)) +
		strconv.Itoa(int(rand.Uint32() % 10)) + strconv.Itoa(int(rand.Uint32() % 10))
}