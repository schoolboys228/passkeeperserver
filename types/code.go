package types

import (
	"gopkg.in/mgo.v2/bson"
	"time"
	"../database"
	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/qr"
	"image/png"
	"log"
	"encoding/base64"
	"bytes"
)

type QRCodeLink struct {
	Id 		bson.ObjectId 	`bson:"_id,omitempty" json:"_id,omitempty"`
	Uuid 		string 		`bson:"uuid"          json:"uuid"`
	ExpiresAt 	time.Time 	`bson:"expires_at"    json:"expires_at"`
	UserID 		bson.ObjectId 	`bson:"user_id"       json:"user_id"`
}


func (cl *QRCodeLink) Valid() (*QRCodeLink, bool){
	err := database.Links.Find(bson.M{"uuid": cl.Uuid}).One(&cl)
	log.Printf("%+v", cl)
	if err != nil {
		panic(err)
		return cl, false
	}
	return cl, true
}

func (cl *QRCodeLink) GenerateQRCode() string {
	var buf = new(bytes.Buffer)
	qc, _ := qr.Encode("https://ramb.onmypc.org:5000/qrcode/" + cl.Uuid, qr.L, qr.Auto)
	qc, _ = barcode.Scale(qc, 500, 500)
	png.Encode(buf, qc)
	img_base64 := base64.StdEncoding.EncodeToString(buf.Bytes())
	return img_base64

}
