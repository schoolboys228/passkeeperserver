package types

import (
	"gopkg.in/mgo.v2/bson"
	"../database/"
	"bytes"
	"github.com/boombuler/barcode/qr"
	"github.com/boombuler/barcode"
	"image/png"
	"encoding/base64"
	"time"
)

type AttachImei struct {
	Id 		bson.ObjectId 	`bson:"_id,omitempty" json:"_id,omitempty"`
	Uuid 		string 		`bson:"uuid"          json:"uuid"`
	UserID 		bson.ObjectId 	`bson:"user_id"       json:"user_id"`
	Imei 		string 		`bson:"imei"          json:"imei"`
	ExpiresAt 	time.Time 	`bson:"expires_at"    json:"-"`
}

func (ai *AttachImei) Valid() (*AttachImei, error) {
	err := database.AttachImei.Find(bson.M{"uuid": ai.Uuid}).One(&ai)
	return ai, err
}

func (ai *AttachImei) SetImeiToUser() (error){
	err := database.Users.UpdateId(ai.UserID, bson.M{"$set": bson.M{"imei": ai.Imei}})
	return err
}

func (ai *AttachImei) GenerateQRCode() string {
	var buf = new(bytes.Buffer)
	qc, _ := qr.Encode("https://ramb.onmypc.org:5000/web/attach_imei/" + ai.Uuid, qr.L, qr.Auto)
	qc, _ = barcode.Scale(qc, 500, 500)
	png.Encode(buf, qc)
	img_base64 := base64.StdEncoding.EncodeToString(buf.Bytes())
	return img_base64

}