package types

import "gopkg.in/mgo.v2/bson"

type Entry struct {
	Id 		bson.ObjectId 	`bson:"_id,omitempty" json:"id"`
	Site 		string 		`bson:"site"          json:"site"`
	Login 		string 		`bson:"login"         json:"login"`
	Password 	string 		`bson:"password"      json:"password"`
}

