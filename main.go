package main

import(
	"log"
	"net/http"
	"./database"
	"./helpers"
)

func main() {
	log.Println("App started")

	database.InitDatabase()
	defer database.Session.Close()

	go helpers.Maintain()

	//helpers.SendMail("zeebobik@gmail.com", "Ты отчислен")

	log.Fatal(http.ListenAndServeTLS(":5000", "server.pem", "server.key", getRouter()))
}
