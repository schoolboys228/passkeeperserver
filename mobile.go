package main

import (
	"net/http"
	"./types"
	"./database"
	"./helpers"
	"gopkg.in/mgo.v2/bson"
	"encoding/json"
	"time"
)

func mobileLogin(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w)



	var user types.User
	helpers.ParseForm(r, &user)

	user.HashPassword()

	if err := database.Users.Find(bson.M{"login":user.Login, "password": user.Password}).One(&user); err != nil {
		json.NewEncoder(w).Encode(Response{Success:false, Message:"Invalid user credentials"}); return
	}

	token := types.Token{Token:helpers.NewUUID(), Type:types.MOBILE,
		UserID:user.Id, ExpiresAt:helpers.GetExpiresAt(time.Hour)}
	database.Tokens.Insert(&token)

	json.NewEncoder(w).Encode(Response{Success:true, Data:token})
}


func mobileOneTimePassword(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w)

	var token types.Token
	helpers.ParseForm(r, &token)

	if _, err := token.Valid(types.MOBILE); err != nil {
		json.NewEncoder(w).Encode(Response{Success:false, Message:"Invalid token"}); return
	}

	var pass types.OneTimePass
	pass.Gen()
	pass.UserId = token.UserID
	pass.ExpiresAt = helpers.GetExpiresAt(time.Hour)

	database.OneTimeCodes.Insert(&pass)

	json.NewEncoder(w).Encode(Response{Success:true, Data:pass})

}