package main

import (
	"net/http"
	"./types"
	"./database"
	"encoding/json"
	"gopkg.in/mgo.v2/bson"
	"time"
	"./helpers"
)

func addTestEntries(w http.ResponseWriter, r *http.Request) {
	
	e1 := types.Entry{Login:"Dima", Password:"Lalka", Site:"Dotka"}
	database.Entries.Insert(&e1)

	e2 := types.Entry{Login:"Kolya", Password:"Doter", Site:"Keq"}
	database.Entries.Insert(&e2)

	e3 := types.Entry{Login:"Yura", Password:"Samosval", Site:"Youtube"}
	database.Entries.Insert(&e3)

	json.NewEncoder(w).Encode(Response{Success:true, Message:"Данные успешно вставлены в базу данных"})
}

func desktopGetAllEntries(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w)
	var token types.Token
	helpers.ParseForm(r, &token)

	if _, err := token.Valid(types.DESKTOP); err != nil {
		json.NewEncoder(w).Encode(Response{Success:false, Message:"Invalid token"})
		return
	}

	var entries []types.Entry
	database.Entries.Find(bson.M{}).All(&entries)
	json.NewEncoder(w).Encode(Response{Success:true, Data:entries})
}

func desktopLogin(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w)

	var user types.User
	helpers.ParseForm(r, &user)

	user.HashPassword()

	helpers.LogObject(user)

	if err := database.Users.Find(bson.M{"login": user.Login, "password": user.Password}).One(&user); err != nil {
		json.NewEncoder(w).Encode(Response{Success:false, Message:"Incorrect user credentials"})
		return
	}

	token := types.Token{Token:helpers.NewUUID(), ExpiresAt:helpers.GetExpiresAt(time.Hour),
		Type:types.DESKTOP, UserID:user.Id}
	database.Tokens.Insert(&token)

	json.NewEncoder(w).Encode(Response{Success:true, Data:token})
}

func desktopAddEntry(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w)

	var token types.Token
	helpers.ParseFormUnsafe(r, &token)

	if _, err := token.Valid(types.DESKTOP); err != nil {
		json.NewEncoder(w).Encode(Response{Success:false, Message:"Invalid token"}); return
	}

	var entry types.Entry

	helpers.ParseFormUnsafe(r, &entry)
	database.Entries.Insert(&entry)

	json.NewEncoder(w).Encode(Response{Success:true})
}

func desktopEditEntry(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w)

	var token types.Token
	helpers.ParseFormUnsafe(r, &token)

	if _, err := token.Valid(types.DESKTOP); err != nil {
		json.NewEncoder(w).Encode(Response{Success:false, Message:"Invalid token"}); return
	}

	var entry types.Entry
	helpers.ParseFormUnsafe(r, &entry)

	if err := database.Entries.Find(bson.M{"_id": entry.Id}); err != nil {
		json.NewEncoder(w).Encode(Response{Success:false, Message:"Invalid entry id"}); return
	}

	database.Entries.UpdateId(entry.Id, bson.M{"$set": bson.M{
		"login": entry.Login,
		"password": entry.Password,
		"site": entry.Site},
	})
	json.NewEncoder(w).Encode(Response{Success:true})
}

func desktopDeleteEntry(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w)

	var(
		token types.Token
		entry types.Entry
	)

	helpers.ParseFormUnsafe(r, &token)
	if _, err := token.Valid(types.DESKTOP); err != nil {
		json.NewEncoder(w).Encode(Response{Success:false, Message:"Invalid token"}); return
	}

	helpers.ParseFormUnsafe(r, &entry)
	if err := database.Entries.FindId(entry.Id); err != nil {
		json.NewEncoder(w).Encode(Response{Success:false, Message:"Invalid entry id"}); return
	}

	database.Entries.RemoveId(entry.Id)
	json.NewEncoder(w).Encode(Response{Success:true})
}