package helpers

import (
	"log"
	"net/http"
	"github.com/gorilla/schema"
	"github.com/satori/go.uuid"
	"time"
	"../database"
	"../types"
	"gopkg.in/mgo.v2/bson"
	"reflect"
)

func ParseForm(r *http.Request, entry interface{}){
	if err := r.ParseForm(); err != nil {
		panic(err)
	}
	log.Printf("%+v\n", r.Form)

	decoder := schema.NewDecoder()
	if err := decoder.Decode(entry, r.Form); err != nil {
		panic(err)
	}
	log.Printf("%+v\n", entry)
}

func ParseFormUnsafe(r *http.Request, entry interface{}){
	if err := r.ParseForm(); err != nil {
		panic(err)
	}
	log.Printf("%+v\n", r.Form)

	schema.NewDecoder().Decode(entry, r.Form)
	log.Printf("%+v\n", entry)
}


func NewUUID() string {
	uuid := uuid.NewV4()
	return uuid.String()
}

func GetExpiresAt(plus time.Duration) time.Time {
	return time.Now().Add(plus)
}

func LogObject(obj interface{}) {
	log.Printf("%s %+v", reflect.TypeOf(obj), obj)
}

func Maintain() {
	var tokens []types.Token
	var imeis []types.AttachImei
	now := time.Now()
	for {
		database.Tokens.Find(bson.M{"expires_at": bson.M{"$lte": now} }).All(&tokens)
		for _, token := range tokens {
			database.Tokens.Remove(bson.M{"_id": token.Id})
			log.Printf("Token deleted %+v\n", token)
		}
		time.Sleep(time.Second * 30)
		database.AttachImei.Find(bson.M{"expires_at": bson.M{"$lte": now}}).All(&imeis)
		for _, link := range imeis {
			database.AttachImei.RemoveId(link.Id)
			log.Printf("AttachImei link deleted %+v\n", link)
		}
		time.Sleep(time.Second * 30)
	}
}

func SetHeaders(w http.ResponseWriter) (http.ResponseWriter){
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	return w
}
