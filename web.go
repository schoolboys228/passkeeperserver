package main

import (
	"net/http"
	"encoding/json"
	"./types"
	"./database"
	"./helpers"
	"gopkg.in/mgo.v2/bson"
	"github.com/gorilla/websocket"
	"github.com/gorilla/mux"
	"time"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize: 1024,
	WriteBufferSize: 1024,
}

func webCheck(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w)

	json.NewEncoder(w).Encode(Response{Success:true})
}

func webToken(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w)

	var user types.User
	helpers.ParseForm(r, &user)
	user.HashPassword()


	if c,_ := database.Users.Find(bson.M{"login": user.Login}).Count(); c != 1 {
		json.NewEncoder(w).Encode(Response{Success:false,Message:"No user with this login"})
		return
	}

	e := database.Users.Find(bson.M{"login":user.Login, "password": user.Password}).One(&user)
	if e != nil {
		json.NewEncoder(w).Encode(Response{Success:false,Message:"Wrong password"})
		return
	}

	token := types.Token{Token: helpers.NewUUID(), UserID:user.Id, 
		ExpiresAt:helpers.GetExpiresAt(time.Hour), Type:types.WEB}
	tokens := database.DB.C("tokens")
	tokens.Insert(&token)

	json.NewEncoder(w).Encode(Response{Success:true,Data:token})
}

func webUser(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w)

	var token types.Token
	helpers.ParseForm(r, &token)

	if _, err := token.Valid(types.WEB); err != nil {
		json.NewEncoder(w).Encode(Response{Success:false, Message:"Invalid token"}); return
	}

	var user types.User
	err := database.Users.Find(bson.M{"_id": token.UserID}).One(&user)

	if err == nil {
		json.NewEncoder(w).Encode(Response{Success:true,Data:user}); return
	} else {
		json.NewEncoder(w).Encode(Response{Success:false,Message:"Invalid token"}); return
	}

}

func webRegister(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w)

	var user types.User
	helpers.ParseForm(r, &user)

	if !user.CheckUnique() {
		json.NewEncoder(w).Encode(Response{Success:false,Message:"User data is not unique"}); return
	}
	if user.Login == "" || user.Email == "" {
		json.NewEncoder(w).Encode(Response{Success:false, Message:"Required user credentrials are empty"}); return
	}

	user.HashPassword()

	database.Users.Insert(&user)

	json.NewEncoder(w).Encode(Response{ Success:true, })
}

func webAttachImei(w http.ResponseWriter, r *http.Request) {
	uuid := mux.Vars(r)["uuid"]
	var req struct{
		Imei string `schema:"imei"`
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	var attImei types.AttachImei
	err := database.AttachImei.Find(bson.M{"uuid": uuid}).One(&attImei)
	if err != nil {
		json.NewEncoder(w).Encode(Response{Success:false, Message:"Incorrent UUID"}); return
	}

	helpers.ParseForm(r, &req)
	//TODO: Implement IMEI verification
	attImei.Imei = req.Imei
	attImei.SetImeiToUser()

	json.NewEncoder(w).Encode(Response{Success:true})
}

func webSocketHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		panic(err)
	}
	for i:=0;i<5;i++ {
		if err := conn.WriteJSON(`{"user":"lalka"}`); err != nil {
			panic(err)
		}
	}
	conn.Close()
}

func webGetQRCode(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w)

	var token types.Token
	helpers.ParseForm(r, &token)
	_, _ = token.Valid(types.WEB)
	helpers.LogObject(token)

	user := token.GetUser()
	helpers.LogObject(user)
	ai := types.AttachImei{Uuid:helpers.NewUUID(), UserID:user.Id,
		ExpiresAt: helpers.GetExpiresAt(time.Minute)}

	database.AttachImei.Insert(&ai)
	if _, err := ai.Valid(); err != nil{
		return
	}
	helpers.LogObject(ai)
	json.NewEncoder(w).Encode(Response{Success:true, Data: ai.GenerateQRCode()})
}

func webLogout(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w)
	//TODO: Check valid
	var token types.Token
	helpers.ParseForm(r, &token)

	if err := database.Tokens.Find(bson.M{"token": token.Token, "type": types.WEB}).One(&token); err != nil {
		json.NewEncoder(w).Encode(Response{Success:false, Message:"Invalid token"}); return
	}

	database.Tokens.RemoveId(token.Id);
	json.NewEncoder(w).Encode(Response{Success:true})
}

func webCheckToken(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w)
	//TODO: Check valid
	var token types.Token
	helpers.ParseForm(r, &token)

	if err := database.Tokens.Find(bson.M{"token": token.Token, "type": types.WEB}).One(&token); err != nil {
		json.NewEncoder(w).Encode(Response{Success:false}); return
	}

	json.NewEncoder(w).Encode(Response{Success:true})
}


func webTestFunc(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w)


	var token types.Token
	var user types.User

	helpers.ParseFormUnsafe(r, &token)
	helpers.ParseFormUnsafe(r, &user)

	json.NewEncoder(w).Encode(Response{Success:true})
}