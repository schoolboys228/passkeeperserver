package database

import (
	"gopkg.in/mgo.v2"
)


var Session 	 *mgo.Session
var DB 		 *mgo.Database

var Users	 *mgo.Collection
var Tokens 	 *mgo.Collection
var Links 	 *mgo.Collection
var AttachImei   *mgo.Collection
var Entries 	 *mgo.Collection
var OneTimeCodes *mgo.Collection


func InitDatabase() (*mgo.Session, *mgo.Database){
	var err error
	if Session, err = mgo.Dial("localhost"); err != nil {
		panic(err)
	}

	Session.SetMode(mgo.Monotonic, true)

	DB = Session.DB("passKeeper")

	InitUsers()
	InitTokens()
	InitLinks()
	InitAttachImei()
	InitEntries()
	InitOneTimeCodes()

	return Session, DB
}

func InitUsers() (*mgo.Collection){
	Users = DB.C("users")

	return Users
}

func InitTokens() (*mgo.Collection){
	Tokens = DB.C("tokens")

	return Tokens
}

func InitLinks() (*mgo.Collection) {
	Links = DB.C("links")

	return Links
}

func InitAttachImei() (*mgo.Collection) {
	AttachImei = DB.C("attach_imei")

	return AttachImei
}

func InitEntries() (*mgo.Collection) {
	Entries = DB.C("entries")


	return Entries
}

func InitOneTimeCodes() (*mgo.Collection) {
	OneTimeCodes = DB.C("one_time_codes")

	return OneTimeCodes
}