/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var React = __webpack_require__(1);
	var ReactDOM = __webpack_require__(2);
	var Main_1 = __webpack_require__(3);
	ReactDOM.render(React.createElement(Main_1.Main, null), document.getElementById("view"));


/***/ },
/* 1 */
/***/ function(module, exports) {

	module.exports = React;

/***/ },
/* 2 */
/***/ function(module, exports) {

	module.exports = ReactDOM;

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var React = __webpack_require__(1);
	var Login_1 = __webpack_require__(4);
	var Header_1 = __webpack_require__(6);
	var User_1 = __webpack_require__(7);
	var Net_1 = __webpack_require__(8);
	var Register_1 = __webpack_require__(9);
	var QRCode_1 = __webpack_require__(11);
	'use strict';
	var WindowStatus;
	(function (WindowStatus) {
	    WindowStatus[WindowStatus["Login"] = 0] = "Login";
	    WindowStatus[WindowStatus["Register"] = 1] = "Register";
	    WindowStatus[WindowStatus["RegisterMobile"] = 2] = "RegisterMobile";
	    WindowStatus[WindowStatus["User"] = 3] = "User";
	})(WindowStatus = exports.WindowStatus || (exports.WindowStatus = {}));
	var View = (function () {
	    function View(p) {
	        var _this = this;
	        this.getView = function () {
	            return _this.view;
	        };
	        this.setView = function (v) {
	            _this.view = v;
	            _this.parent.forceUpdate();
	        };
	        this.parent = p;
	        this.view = WindowStatus.Login;
	    }
	    return View;
	}());
	exports.View = View;
	var Main = (function (_super) {
	    __extends(Main, _super);
	    function Main(props) {
	        var _this = _super.call(this, props) || this;
	        _this.handleLoginSuccessed = function () {
	            _this.state.view.setView(WindowStatus.User);
	            _this.forceUpdate();
	        };
	        _this.handleRegisterDone = function () {
	            _this.state.view.setView(WindowStatus.Login);
	            _this.forceUpdate();
	        };
	        _this.state = {
	            requester: new Net_1.Requester(),
	            view: new View(_this)
	        };
	        return _this;
	    }
	    ;
	    Main.prototype.componentDidMount = function () {
	        this.state.requester.init();
	    };
	    Main.prototype.renderLoginView = function () {
	        var _this = this;
	        if (this.state.view.getView() == WindowStatus.Login) {
	            return (React.createElement(Login_1.LoginView, { ref: function (c) { return _this.loginView = c; }, loginSuccessed: this.handleLoginSuccessed, requster: this.state.requester }));
	        }
	        else
	            return null;
	    };
	    Main.prototype.renderRegisterView = function () {
	        var _this = this;
	        if (this.state.view.getView() == WindowStatus.Register) {
	            return (React.createElement(Register_1.RegisterView, { ref: function (c) { return _this.registerView = c; }, registerSuccessed: this.handleRegisterDone, requester: this.state.requester }));
	        }
	        else
	            return null;
	    };
	    Main.prototype.renderUserView = function () {
	        var _this = this;
	        if (this.state.view.getView() == WindowStatus.User) {
	            return (React.createElement(User_1.UserView, { ref: function (c) { return _this.userView = c; }, requester: this.state.requester }));
	        }
	        else
	            return null;
	    };
	    Main.prototype.renderQRCodeView = function () {
	        if (this.state.view.getView() == WindowStatus.RegisterMobile) {
	            return (React.createElement(QRCode_1.QRCodeView, { requester: this.state.requester }));
	        }
	    };
	    Main.prototype.render = function () {
	        return (React.createElement("div", null,
	            React.createElement(Header_1.Header, { requester: this.state.requester, view: this.state.view }),
	            React.createElement("div", { className: 'container-fluid' },
	                this.renderLoginView(),
	                this.renderRegisterView(),
	                this.renderUserView(),
	                this.renderQRCodeView())));
	    };
	    return Main;
	}(React.Component));
	exports.Main = Main;


/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var React = __webpack_require__(1);
	var Button_1 = __webpack_require__(5);
	var LoginView = (function (_super) {
	    __extends(LoginView, _super);
	    function LoginView(props) {
	        var _this = _super.call(this, props) || this;
	        _this.handleLogin = function () {
	            var params = {
	                login: _this.state.login,
	                password: _this.state.password
	            };
	            _this.props.requster.getToken(params, function (error, result) {
	                if (error == null) {
	                    _this.props.loginSuccessed();
	                }
	                ;
	            });
	        };
	        _this.handleLoginField = function (e) {
	            _this.setState({
	                login: e.target.value,
	                password: _this.state.password
	            });
	        };
	        _this.handlePasswordField = function (e) {
	            _this.setState({
	                login: _this.state.login,
	                password: e.target.value
	            });
	        };
	        _this.state = {
	            login: '',
	            password: ''
	        };
	        return _this;
	    }
	    LoginView.prototype.componentWillReceiveProps = function (props) {
	    };
	    LoginView.prototype.render = function () {
	        var _this = this;
	        return (React.createElement("form", { className: 'login-form', role: 'form' },
	            React.createElement("h2", null, "Please sign in"),
	            React.createElement("input", { type: 'email', className: 'form-control', placeholder: 'Login', onChange: function (e) { return _this.handleLoginField(e); } }),
	            React.createElement("input", { type: 'password', className: 'form-control', placeholder: 'Password', onChange: function (e) { return _this.handlePasswordField(e); } }),
	            React.createElement(Button_1.Button, { onClick: this.handleLogin, text: 'Sign in' })));
	    };
	    return LoginView;
	}(React.Component));
	exports.LoginView = LoginView;


/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var React = __webpack_require__(1);
	var Button = (function (_super) {
	    __extends(Button, _super);
	    function Button() {
	        return _super.call(this) || this;
	    }
	    Button.prototype.render = function () {
	        return (React.createElement("div", { className: 'btn btn-lg btn-primary btn-block' + this.props.className, onClick: this.props.onClick }, this.props.text));
	    };
	    return Button;
	}(React.Component));
	exports.Button = Button;


/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var React = __webpack_require__(1);
	var Main_1 = __webpack_require__(3);
	var Header = (function (_super) {
	    __extends(Header, _super);
	    function Header(props) {
	        var _this = _super.call(this, props) || this;
	        _this.goLogin = function () {
	            _this.props.view.setView(Main_1.WindowStatus.Login);
	        };
	        _this.goRegister = function () {
	            _this.props.view.setView(Main_1.WindowStatus.Register);
	        };
	        _this.goLogout = function () {
	            _this.props.requester.removeToken(function (error, result) {
	                if (error == null) {
	                    if (result == true) {
	                        _this.props.view.setView(Main_1.WindowStatus.Login);
	                    }
	                }
	            });
	        };
	        _this.goQRCode = function () {
	            _this.props.view.setView(Main_1.WindowStatus.RegisterMobile);
	        };
	        return _this;
	    }
	    Header.prototype.renderLinks = function () {
	        if ((this.props.view.getView() == Main_1.WindowStatus.Login) || (this.props.view.getView() == Main_1.WindowStatus.Register)) {
	            return (React.createElement("ul", { className: 'nav navbar-nav navbar-right' },
	                React.createElement("li", { onClick: this.goLogin },
	                    React.createElement("a", { href: '#' }, "Login")),
	                React.createElement("li", { onClick: this.goRegister },
	                    React.createElement("a", { href: '#' }, "Register"))));
	        }
	        else {
	            return (React.createElement("ul", { className: 'nav navbar-nav navbar-right' },
	                React.createElement("li", { onClick: this.goQRCode },
	                    React.createElement("a", { href: '#' }, "Attach mobile phone")),
	                React.createElement("li", { onClick: this.goLogout },
	                    React.createElement("a", { href: '#' }, "Logout"))));
	        }
	    };
	    Header.prototype.componentWillReceiveProps = function (props) {
	    };
	    Header.prototype.render = function () {
	        return (React.createElement("nav", { className: 'navbar navbar-default navbar-static-top', role: 'navigation' },
	            React.createElement("div", { className: 'container' },
	                React.createElement("a", { className: 'navbar-brand' }, "THE SHIT (and we call it passKeeperWeb)"),
	                this.renderLinks())));
	    };
	    return Header;
	}(React.Component));
	exports.Header = Header;


/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var React = __webpack_require__(1);
	var Button_1 = __webpack_require__(5);
	var UserView = (function (_super) {
	    __extends(UserView, _super);
	    function UserView(props) {
	        var _this = _super.call(this, props) || this;
	        _this.state = {
	            user: {
	                login: '',
	                email: '',
	                password: ''
	            }
	        };
	        return _this;
	    }
	    UserView.prototype.componentDidMount = function () {
	        var _this = this;
	        this.props.requester.getInfo(function (error, result) {
	            _this.setState({
	                user: result
	            });
	        });
	    };
	    UserView.prototype.componentWillReceiveProps = function (props) {
	    };
	    UserView.prototype.render = function () {
	        return (React.createElement("div", { className: 'center' },
	            React.createElement("div", { className: 'user-view highlight' },
	                React.createElement("h1", null, "User's profile"),
	                React.createElement("table", { className: 'table table-hover' },
	                    React.createElement("tbody", null,
	                        React.createElement("tr", null,
	                            React.createElement("td", null, "Login"),
	                            React.createElement("td", null, this.state.user.login)),
	                        React.createElement("tr", null,
	                            React.createElement("td", null, "E-mail"),
	                            React.createElement("td", null, this.state.user.email)),
	                        React.createElement("tr", null,
	                            React.createElement("td", null, "Imei"),
	                            React.createElement("td", null, this.state.user.imei)))),
	                React.createElement(Button_1.Button, { text: 'Koq' }))));
	    };
	    return UserView;
	}(React.Component));
	exports.UserView = UserView;


/***/ },
/* 8 */
/***/ function(module, exports) {

	"use strict";
	var Requester = (function () {
	    function Requester() {
	        var _this = this;
	        this.validToken = false;
	        this.init = function () {
	            _this.token = localStorage.getItem('token');
	            if (_this.token != null) {
	                _this.checkToken(function (err, res) {
	                    if (res == false) {
	                        _this.token = null;
	                        _this.validToken = false;
	                    }
	                    else {
	                        _this.validToken = true;
	                    }
	                });
	            }
	        };
	        this.checkToken = function (callback) {
	            $.ajax({
	                url: '/web/checkToken',
	                type: 'POST',
	                data: {
	                    token: _this.token
	                }
	            }).done(function (response) {
	                if (response.success == true) {
	                    _this.token = response.data.token;
	                    localStorage.setItem('token', _this.token);
	                    callback(null, true);
	                }
	                else
	                    callback('Invalid token', false);
	            }).fail(function (error) {
	                callback('Network error', false);
	            });
	        };
	        this.getToken = function (params, callback) {
	            var result = null;
	            $.ajax({
	                url: '/web/token',
	                type: 'POST',
	                data: {
	                    login: params.login,
	                    password: params.password
	                }
	            }).done(function (response) {
	                if (response.success == true) {
	                    _this.token = response.data.token;
	                    localStorage.setItem('token', _this.token);
	                    callback(null, response.data);
	                }
	                else
	                    callback("Invalid user's credentials", response.data);
	            }).fail(function (error) {
	                callback('Network error', null);
	            });
	        };
	        this.getInfo = function (callback) {
	            $.ajax({
	                url: '/web/user',
	                type: 'POST',
	                data: {
	                    token: _this.token
	                }
	            }).done(function (response) {
	                if (response.success == true)
	                    callback(null, response.data);
	                else
	                    callback(response.error, null);
	            }).fail(function () {
	                callback('Network error', null);
	            });
	        };
	        this.removeToken = function (callback) {
	            $.ajax({
	                url: '/web/logout',
	                type: 'POST',
	                data: {
	                    token: _this.token
	                }
	            }).done(function (response) {
	                if (response.success == true)
	                    callback(null, true);
	                else
	                    callback(response.error, false);
	                localStorage.removeItem('token');
	            }).fail(function () {
	                callback('Network error', false);
	            });
	        };
	        this.registerNew = function (params, callback) {
	            $.ajax({
	                url: '/web/register',
	                type: 'POST',
	                data: {
	                    login: params.login,
	                    email: params.email,
	                    password: params.password
	                }
	            }).done(function (response) {
	                if (response.success == true)
	                    callback(null, true);
	                else
	                    callback(response.error, false);
	            }).fail(function () {
	                callback('Network error', false);
	            });
	        };
	        this.getQRCode = function (callback) {
	            $.ajax({
	                url: '/test/qr',
	                type: 'POST',
	                data: {
	                    token: _this.token
	                }
	            }).done(function (response) {
	                if (response.success == true)
	                    callback(null, response.data);
	                else
	                    callback(response.error, null);
	            }).fail(function () {
	                callback('Network error', null);
	            });
	        };
	    }
	    return Requester;
	}());
	exports.Requester = Requester;


/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var React = __webpack_require__(1);
	var Button_1 = __webpack_require__(5);
	var Functions_1 = __webpack_require__(10);
	var RegisterView = (function (_super) {
	    __extends(RegisterView, _super);
	    function RegisterView(props) {
	        var _this = _super.call(this, props) || this;
	        _this.handleLoginChange = function (e) {
	            _this.setState({
	                login: e.target.value
	            });
	        };
	        _this.handleEmailChange = function (e) {
	            _this.setState({
	                email: e.target.value
	            });
	        };
	        _this.handlePasswordChange = function (e) {
	            _this.setState({
	                password: e.target.value
	            }, _this.checkDifferentPasswords);
	            _this.checkDifferentPasswords();
	        };
	        _this.handleVPasswordChange = function (e) {
	            _this.setState({
	                v_password: e.target.value
	            }, _this.checkDifferentPasswords);
	        };
	        _this.checkDifferentPasswords = function () {
	            _this.setState({
	                error: _this.state.password != _this.state.v_password ? 'Different passwords' : ''
	            });
	        };
	        _this.handleRegisterButton = function () {
	            var error = '';
	            if (_this.state.login === '') {
	                error += 'Need login';
	            }
	            if (!Functions_1.validateEmail(_this.state.email)) {
	                error += 'Need valid email';
	            }
	            if (_this.state.password !== _this.state.v_password) {
	                error += 'Passwords are not eqaul';
	            }
	            _this.setState({
	                error: error
	            });
	            if (error !== '')
	                return;
	            var params = {
	                login: _this.state.login,
	                email: _this.state.email,
	                password: _this.state.password
	            };
	            _this.props.requester.registerNew(params, function (error, result) {
	                if (error == null) {
	                    if (result == true) {
	                        _this.props.registerSuccessed();
	                    }
	                }
	            });
	        };
	        _this.state = {
	            login: '',
	            email: '',
	            password: '',
	            v_password: '',
	            error: ''
	        };
	        return _this;
	    }
	    RegisterView.prototype.componentDidUpdate = function () {
	    };
	    RegisterView.prototype.componentWillReceiveProps = function (props) {
	    };
	    RegisterView.prototype.render = function () {
	        return (React.createElement("div", null,
	            React.createElement("div", { className: 'register-form' },
	                React.createElement("h2", null, "Register"),
	                React.createElement("input", { type: 'login', placeholder: 'Login', className: 'form-control', onChange: this.handleLoginChange }),
	                React.createElement("input", { type: 'email', placeholder: 'E-mail', className: 'form-control', onChange: this.handleEmailChange }),
	                React.createElement("input", { type: 'password', placeholder: 'Password', className: 'form-control', onChange: this.handlePasswordChange }),
	                React.createElement("input", { type: 'password', placeholder: 'Verify password', className: 'form-control', onChange: this.handleVPasswordChange }),
	                React.createElement(Button_1.Button, { text: 'Register', onClick: this.handleRegisterButton }),
	                React.createElement("p", { className: 'error-message center bg-danger' }, this.state.error))));
	    };
	    return RegisterView;
	}(React.Component));
	exports.RegisterView = RegisterView;


/***/ },
/* 10 */
/***/ function(module, exports) {

	"use strict";
	exports.validateEmail = function (email) {
	    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	};


/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var React = __webpack_require__(1);
	var Button_1 = __webpack_require__(5);
	var QRCView;
	(function (QRCView) {
	    QRCView[QRCView["Request"] = 0] = "Request";
	    QRCView[QRCView["Result"] = 1] = "Result";
	})(QRCView || (QRCView = {}));
	var QRCodeView = (function (_super) {
	    __extends(QRCodeView, _super);
	    function QRCodeView(props) {
	        var _this = _super.call(this, props) || this;
	        _this.requestQRCode = function () {
	            var code;
	            _this.props.requester.getQRCode(function (error, result) {
	                if (error == null) {
	                    _this.setState({
	                        view: QRCView.Result,
	                        code: result
	                    });
	                }
	            });
	        };
	        _this.goBack = function () {
	            _this.setState({
	                view: QRCView.Request
	            });
	        };
	        _this.state = {
	            view: QRCView.Request,
	            code: ''
	        };
	        return _this;
	    }
	    QRCodeView.prototype.renderRequest = function () {
	        if (this.state.view != QRCView.Request) {
	            return null;
	        }
	        return (React.createElement("div", { className: 'qrcode' },
	            React.createElement(Button_1.Button, { text: 'Get QR Code', onClick: this.requestQRCode, className: 'qrcode--button' })));
	    };
	    QRCodeView.prototype.renderResult = function () {
	        if (this.state.view != QRCView.Result) {
	            return null;
	        }
	        return (React.createElement("div", { className: 'qrcode center' },
	            React.createElement("div", { dangerouslySetInnerHTML: { __html: "<img src=\"data:image/png;base64," + this.state.code + "\">"
	                } }),
	            React.createElement(Button_1.Button, { text: 'Back', onClick: this.goBack, className: 'qrcode--button' })));
	    };
	    QRCodeView.prototype.componentWillReceiveProps = function (props) {
	    };
	    QRCodeView.prototype.render = function () {
	        return (React.createElement("div", null,
	            this.renderRequest(),
	            this.renderResult()));
	    };
	    return QRCodeView;
	}(React.Component));
	exports.QRCodeView = QRCodeView;


/***/ }
/******/ ]);
//# sourceMappingURL=bundle.js.map