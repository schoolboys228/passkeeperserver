package main

import(
	"github.com/gorilla/mux"
	"net/http"
)

func getRouter() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/web/check",	 	webCheck)
	r.HandleFunc("/web/token",	 	webToken)
	r.HandleFunc("/web/user",	 	webUser)
	r.HandleFunc("/web/register",	 	webRegister)
	r.HandleFunc("/web/ws", 	 	webSocketHandler)
	r.HandleFunc("/test/qr", 	 	webGetQRCode)
	r.HandleFunc("/web/logout", 	 	webLogout)
	r.HandleFunc("/web/checkToken",		webCheckToken)
	r.HandleFunc("/web/attach_imei/{uuid}", webAttachImei)
	r.HandleFunc("/web/koq", 		webTestFunc)

	r.HandleFunc("/desktop/addTestData", 	addTestEntries) // for testing only
	r.HandleFunc("/desktop/getAll", 	desktopGetAllEntries) // for testing only
	r.HandleFunc("/desktop/login", 		desktopLogin) // for testing only
	r.HandleFunc("/desktop/add", 		desktopAddEntry)
	r.HandleFunc("/desktop/edit", 		desktopEditEntry)
	r.HandleFunc("/desktop/remove", 	desktopDeleteEntry)

	r.HandleFunc("/mobile/login", 		mobileLogin)
	r.HandleFunc("/mobile/code",            mobileOneTimePassword)

	r.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(http.Dir("static/"))))
	return r
}